# MMIX System Build #

## Reference ##

[mmix getting started](http://mmix.cs.hm.edu/getstarted.html)

## This Project ##

This project contains a complete build of the MMIX environment.  Built
on UBUNTU 14.04.4 system.

Binaries are included in the /bin directory.  They will probably run on
almost any Linux system.

However, we recommend you try to build them yourself.

